import * as assert from 'assert';
import { Ed25519, Prime256v1 } from '../root/EllipticCurves';
import { keyPairFactory } from '../root/KeyPairFactory';

describe('KeyPair', () => {
  it('Secp256k1', async () => {
    // const item = Secp256k1.fromHex(
    //   '04e0c9e5ed6f40171dcf9fe52f9a6c2b31c9ca36a18fe1aac9e5f0619b7b2fb545090600635e9570a1beb33cebefbe4b864440e7c35cf2fd17d4bbf0344679fdef'
    // );
    const item = keyPairFactory.createInstance(
      'secp256k1',
      '04e0c9e5ed6f40171dcf9fe52f9a6c2b31c9ca36a18fe1aac9e5f0619b7b2fb545090600635e9570a1beb33cebefbe4b864440e7c35cf2fd17d4bbf0344679fdef'
    );

    assert.strictEqual(
      item.getKeyPairHex().publicKey,
      '03e0c9e5ed6f40171dcf9fe52f9a6c2b31c9ca36a18fe1aac9e5f0619b7b2fb545'
    );

    assert.ok(
      item.isEqual(
        keyPairFactory.createInstance(
          'secp256k1',
          'e0c9e5ed6f40171dcf9fe52f9a6c2b31c9ca36a18fe1aac9e5f0619b7b2fb545090600635e9570a1beb33cebefbe4b864440e7c35cf2fd17d4bbf0344679fdef'
        )
      )
    );
  });

  it('Prime256v1', async () => {
    const keyPair = Prime256v1.make();
  });

  it('Ed2519', async () => {
    const keyPair = Ed25519.make();
  });
});
