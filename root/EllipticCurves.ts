import { EllipticCurveBase } from './Base/EllipticCurveBase';

// @bindConstructor<EllipticCurveConstructor>()
export class Ed25519 extends EllipticCurveBase {
  type() {
    return Ed25519;
  }
}

// @bindConstructor<EllipticCurveConstructor>()
export class Prime192v1 extends EllipticCurveBase {
  type() {
    return Prime192v1;
  }
}

// @bindConstructor<EllipticCurveConstructor>()
export class Prime256v1 extends EllipticCurveBase {
  type() {
    return Prime256v1;
  }
}

// @bindConstructor<EllipticCurveConstructor>()
export class Secp224r1 extends EllipticCurveBase {
  type() {
    return Secp224r1;
  }
}

// @bindConstructor<EllipticCurveConstructor>()
export class Secp256k1 extends EllipticCurveBase {
  type() {
    return Secp256k1;
  }
}

// @bindConstructor<EllipticCurveConstructor>()
export class Secp384r1 extends EllipticCurveBase {
  type() {
    return Secp384r1;
  }
}

// @bindConstructor<EllipticCurveConstructor>()
export class Secp521r1 extends EllipticCurveBase {
  type() {
    return Secp521r1;
  }
}
