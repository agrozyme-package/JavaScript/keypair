import { AnyConstructor, Base } from '@agrozyme/base-class';
import { KeyPairBuffer, KeyPairHex, KeyPairInterface } from './KeyPairInterface';

import { isDeepStrictEqual } from 'util';

export abstract class KeyPairBase extends Base implements KeyPairInterface {
  static readonly index: string = '';

  protected declare privateKey: Buffer;
  protected declare publicKey: Buffer;

  constructor(data: string, isPrivateKey: boolean = false, ...items: any[]) {
    super(...arguments);
    const item = isPrivateKey ? this.setupPrivateKey(data) : this.setupPublicKey(data);
    this.privateKey = item.privateKey;
    this.publicKey = item.publicKey;
  }

  static algorithm(): string {
    return this.name.toLowerCase();
  }

  static fromBuffer<U extends typeof KeyPairBase>(
    this: U,
    data: Buffer,
    isPrivateKey: boolean = false
  ): AnyConstructor<U> {
    const hex = data.toString('hex');
    return this.fromHex(hex, isPrivateKey);
  }

  static fromHex<U extends typeof KeyPairBase>(
    this: U,
    data: string,
    isPrivateKey: boolean = false
  ): AnyConstructor<U> {
    return this.create(...arguments);
  }

  static getPrivateKeyBytes(): number {
    return 0;
  }

  static isPrivateKey(data: string): boolean {
    let test = true;

    // noinspection UnusedCatchParameterJS
    try {
      this.fromHex(data, true);
    } catch (error) {
      test = false;
    }

    return test;
  }

  static isPublicKey(data: string): boolean {
    let test = true;

    // noinspection UnusedCatchParameterJS
    try {
      this.fromHex(data, false);
    } catch (error) {
      test = false;
    }

    return test;
  }

  static normalizeHex(data: string): string {
    const text = data.trim().toLowerCase();
    return text.startsWith('0x') ? text.slice(2) : text;
  }

  static publicKeyConvert(buffer: Buffer, compressed?: boolean): Buffer {
    return Buffer.from([]);
  }

  getKeyPairBuffer(compressed: boolean = true): KeyPairBuffer {
    return {
      privateKey: this.toBuffer(true, compressed),
      publicKey: this.toBuffer(false, compressed),
    };
  }

  getKeyPairHex(compressed: boolean = true, hasPrefix: boolean = false): KeyPairHex {
    return {
      privateKey: this.toHex(true, compressed, hasPrefix),
      publicKey: this.toHex(false, compressed, hasPrefix),
    };
  }

  isEqual(item: KeyPairInterface): boolean {
    return isDeepStrictEqual(this.getKeyPairBuffer(), item.getKeyPairBuffer());
  }

  type() {
    return KeyPairBase;
  }

  protected abstract setupPrivateKey(data: string): KeyPairBuffer;

  protected abstract setupPublicKey(data: string): KeyPairBuffer;

  protected toBuffer(isPrivateKey: boolean, compressed: boolean = true): Buffer {
    return isPrivateKey ? this.privateKey : this.type().publicKeyConvert(this.publicKey, compressed);
  }

  protected toHex(isPrivateKey: boolean, compressed: boolean = true, hasPrefix: boolean = false): string {
    const hex = this.toBuffer(isPrivateKey, compressed).toString('hex');
    return hasPrefix ? '0x' + hex : hex;
  }
}
