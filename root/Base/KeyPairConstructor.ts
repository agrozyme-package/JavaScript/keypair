import { AnyConstructor } from '@agrozyme/base-class';
import { KeyPairHex, KeyPairInterface } from './KeyPairInterface';

export interface KeyPairConstructor<U extends KeyPairInterface = KeyPairInterface> extends AnyConstructor<U> {
  algorithm(): string;

  fromBuffer(data: Buffer, isPrivateKey?: boolean): U;

  fromHex(data: string, isPrivateKey?: boolean, ...items: any[]): U;

  getPrivateKeyBytes(): number;

  isPrivateKey(data: string): boolean;

  isPublicKey(data: string): boolean;

  make(): KeyPairHex;

  normalizeHex(data: string): string;

  publicKeyConvert(buffer: Buffer, compressed?: boolean): Buffer;

  new (data: string, isPrivateKey?: boolean, ...items: any[]): U;
}
