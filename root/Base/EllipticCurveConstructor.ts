import { EllipticCurve } from './EllipticCurveBase';
import { KeyPairConstructor } from './KeyPairConstructor';
import { KeyPairInterface } from './KeyPairInterface';

export interface EllipticCurveConstructor<U extends KeyPairInterface = KeyPairInterface> extends KeyPairConstructor<U> {
  getEllipticCurve(): EllipticCurve;
}
