import { ECDH } from 'crypto';
import { ec } from 'elliptic';
import { KeyPairBase } from './KeyPairBase';
import { KeyPairBuffer, KeyPairHex } from './KeyPairInterface';

export interface EllipticCurve extends ECDH {
  curve: ec;
  curveType: {
    byteLength: number;
    name: string;
  };
}

export abstract class EllipticCurveBase extends KeyPairBase {
  static getEllipticCurve(): EllipticCurve {
    const createECDH = require('create-ecdh/browser');
    return createECDH(this.algorithm());
  }

  static getPrivateKeyBytes(): number {
    return this.getEllipticCurve().curveType.byteLength;
  }

  static make(): KeyPairHex {
    const item = this.getEllipticCurve();
    const publicKey = item.generateKeys('hex', 'compressed');
    const privateKey = item.getPrivateKey('hex');
    return { privateKey, publicKey };
  }

  static publicKeyConvert(buffer: Buffer, compressed: boolean = true): Buffer {
    const data = this.getEllipticCurve()
      .curve.keyFromPublic(buffer)
      .getPublic(compressed, 'hex');
    return Buffer.from(data, 'hex');
  }

  type() {
    return EllipticCurveBase;
  }

  protected setupPrivateKey(data: string): KeyPairBuffer {
    const privateKey = Buffer.from(data, 'hex');
    const item = this.type().getEllipticCurve();
    item.setPrivateKey(privateKey);
    const hex = item.getPublicKey('hex', 'compressed');
    const publicKey = Buffer.from(hex, 'hex');
    return { privateKey, publicKey };
  }

  protected setupPublicKey(data: string): KeyPairBuffer {
    const type = this.type();
    const hex = type.normalizeHex(data);
    const uncompressed = { bytes: 65, firstByte: 0x04 };

    let buffer = Buffer.from(hex, 'hex');

    if (uncompressed.bytes === buffer.length + 1) {
      const prefix = Buffer.from([uncompressed.firstByte]);
      buffer = Buffer.concat([prefix, buffer]);
    }

    return {
      privateKey: Buffer.from([]),
      publicKey: type.publicKeyConvert(buffer, true)
    };
  }
}
