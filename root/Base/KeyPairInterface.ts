export interface KeyPairInterface {
  isEqual(item: KeyPairInterface): boolean;

  getKeyPairBuffer(compressed?: boolean): KeyPairBuffer;

  getKeyPairHex(compressed?: boolean, withPrefix?: boolean): KeyPairHex;
}

export interface KeyPairHex {
  privateKey: string;
  publicKey: string;
}

export interface KeyPairBuffer {
  privateKey: Buffer;
  publicKey: Buffer;
}

export interface EciesOptions {
  curveName: string;
  keyFormat: 'uncompressed' | 'compressed';
  hashName?: string;
  hashLength?: number;
  macName?: string;
  macLength?: number;
  symmetricCypherName?: string;
  iv?: Buffer;
  s1?: Buffer;
  s2?: Buffer;
}
