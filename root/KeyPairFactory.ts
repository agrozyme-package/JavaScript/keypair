import { Registry } from '@agrozyme/base-class';
import { mapToObject } from 'rambdax';
import { KeyPairConstructor } from './Base/KeyPairConstructor';
import { KeyPairInterface } from './Base/KeyPairInterface';
import { Ed25519, Prime192v1, Prime256v1, Secp224r1, Secp256k1, Secp384r1, Secp521r1 } from './EllipticCurves';
import { KeyPairNull } from './KeyPairNull';

// export interface KeyPairConstructorMap extends ConstructorMap<KeyPairConstructor, KeyPairInterface> {}

// @bindConstructor<Singleton<KeyPairFactory>>()
export class KeyPairFactory implements Registry<KeyPairConstructor, KeyPairInterface> {
  static readonly singleton = new KeyPairFactory();
  declare readonly registry: Record<string, KeyPairConstructor>;

  protected constructor() {
    const items: any[] = [Ed25519, Prime192v1, Prime256v1, Secp224r1, Secp256k1, Secp384r1, Secp521r1];
    this.registry = mapToObject((item) => ({ [item.algorithm()]: item }), items);
  }

  createInstance(name: string, data: string, isPrivateKey: boolean = false): KeyPairInterface {
    return this.getConstructor(name).fromHex(data, isPrivateKey);
  }

  getConstructor(name: string): KeyPairConstructor {
    return this.registry[name] ?? KeyPairNull;
  }
}

export const keyPairFactory = KeyPairFactory.singleton;
