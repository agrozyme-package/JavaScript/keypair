import { KeyPairBase } from './Base/KeyPairBase';
import { KeyPairBuffer, KeyPairHex } from './Base/KeyPairInterface';

// @bindConstructor<KeyPairConstructor>()
export class KeyPairNull extends KeyPairBase {
  // noinspection JSUnusedLocalSymbols
  static isPrivateKey(data: string): boolean {
    return false;
  }

  // noinspection JSUnusedLocalSymbols
  static isPublicKey(data: string): boolean {
    return false;
  }

  // noinspection JSUnusedLocalSymbols
  static publicKeyConvert(buffer: Buffer, compressed: boolean = true): Buffer {
    return Buffer.from([]);
  }

  protected setupPrivateKey(data: string): KeyPairBuffer {
    const buffer = Buffer.from([]);
    return { privateKey: buffer, publicKey: buffer };
  }

  protected setupPublicKey(data: string): KeyPairBuffer {
    const buffer = Buffer.from([]);
    return { privateKey: buffer, publicKey: buffer };
  }

  protected toBuffer(isPrivateKey: boolean, compressed: boolean): Buffer {
    return this.publicKey;
  }

  type() {
    return KeyPairNull;
  }

  static make(): KeyPairHex {
    return { privateKey: '', publicKey: '' };
  }
}
